package com.solomia;

import com.solomia.constants.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class GmailSendLetterPage {
    public WebDriver sendEmail(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get(Constants.InboxURL);
//        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\":3i\"]/div/div"))).click(); робоча хрінь
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.T-I.T-I-KE.L3"))).click();
        WebElement sendToElement = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.className("vO")));
        sendToElement.click();
        sendToElement.clear();
        sendToElement.sendKeys("solomia.lylyk@gmail.com");

        WebElement emailBodyElement = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@role = 'textbox']")));
        emailBodyElement.click();
        emailBodyElement.clear();
        emailBodyElement.sendKeys("Hello");

        WebElement subjectElement = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name = 'subjectbox']")));
        subjectElement.click();
        subjectElement.clear();
        subjectElement.sendKeys("Selenium subject");

        WebElement sendMailElement = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\":86\"]")));
        sendMailElement.click();

        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        return driver;
    }
}
