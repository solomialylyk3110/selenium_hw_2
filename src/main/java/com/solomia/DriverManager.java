package com.solomia;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverManager {
    public WebDriver createDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }
    public void quiteDriver(WebDriver driver) {
        driver.quit();
    }

    public void closeDriver(WebDriver driver) {
        driver.close();
    }

    public WebDriverWait waitDriver(WebDriver driver, int timeOutlnSeconds) {
        return new WebDriverWait(driver, timeOutlnSeconds);
    }
}
