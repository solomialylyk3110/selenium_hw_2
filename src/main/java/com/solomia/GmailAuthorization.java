package com.solomia;

import com.solomia.constants.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailAuthorization {
    public WebDriver logInEmailAccount(WebDriver driver) {
        driver.get(Constants.EmailLogInURL);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='identifierId']"))).sendKeys(Constants.login);
        driver.findElement(By.id("identifierNext")).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='password']"))).sendKeys(Constants.password);
        driver.findElement(By.id("passwordNext")).click();
        System.out.println(driver.getTitle());
        return driver;
    }
}
