package com.solomia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SeleniumSendGmailLetter {
    private static Logger logger = LogManager.getLogger(SeleniumSendGmailLetter.class);
    WebDriver driver = new DriverManager().createDriver();
    WebDriverWait wait = new DriverManager().waitDriver(driver, 10);

    @Test
    public void testLogInAndSendLetter(){
        GmailAuthorization authorization = new GmailAuthorization();
        authorization.logInEmailAccount(driver);
        Assert.assertTrue(driver.getCurrentUrl().contains("https://accounts.google.com") || driver.getCurrentUrl().contains("https://mail.google.com/mail"));

        GmailSendLetterPage gmailSendLetterPage = new GmailSendLetterPage();
        gmailSendLetterPage.sendEmail(driver);
        Assert.assertTrue(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Лист надіслано.')]"))).isEnabled());
    }
}
